(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = require("./logger");
const frida_syscall_interceptor_1 = require("../../frida-syscall-interceptor");
const frida_syscall_interceptor_2 = require("../../frida-syscall-interceptor");
/*
const header = Memory.alloc(16);
header
    .writeU32(0xdeadbeef).add(4)
    .writeU32(0xd00ff00d).add(4)
    .writeU64(uint64("0x1122334455667788"));
log(hexdump(header.readByteArray(16) as ArrayBuffer, { ansi: true }));

Process.getModuleByName("libSystem.B.dylib")
    .enumerateExports()
    .slice(0, 16)
    .forEach((exp, index) => {
        log(`export ${index}: ${exp.name}`);
    });

Interceptor.attach(Module.getExportByName(null, "open"), {
    onEnter(args) {
        const path = args[0].readUtf8String();
        log(`open() path="${path}"`);
    }
});
*/
// Somewhere in your code.
//*
let baseAddr = Module.findBaseAddress('libnative-lib.so');
//指令的地址 这个地址会随着你自己编译的apk变化的，
// 如果hook一次后备份的地址会变化，要重新启动一下应用
// let address = baseAddr.add('0x81F8'); //  00 00 00 EF <= SVC  0

// hook openat /system/bin/su
let address1 = baseAddr.add('0xf1cc');
 //hook openat /system/bin/su
logger_1.log("cpu "+Process.arch);
frida_syscall_interceptor_1.hookSyscall(address1, new NativeCallback(function (dirfd, pathname, mode, flags) {
    let path = pathname.readCString();
    logger_1.log(`Called openat hook` );
    logger_1.log('- R0: ' + dirfd);
    logger_1.log('- R1: ' + path);
    logger_1.log('- R2: ' + mode);
    logger_1.log('- R3: ' + flags);
    return 0;
}, 'int', ['int', 'pointer', 'int', 'int']));

//如果出异常，重启动一下手机，一次只能hook一个函数
//__NR_read, __fd, (long)__buf, (long)__count
// let address2 = baseAddr.add('0x8520');
// let address2 = baseAddr.add('0xF588');
// frida_syscall_interceptor_2.hookSyscall(address2, new NativeCallback(function (dirfd, __buf, __count) {
//     let infor = __buf.readCString();
//     logger_1.log(`Called read hook` );
//     logger_1.log('- R0: ' + dirfd);
//     logger_1.log('- R1: ' + __buf +" string=> "+infor);
//     logger_1.log('- R2: ' + __count);
//     return 0;
// }, 'int', ['int', 'pointer', 'int']));

// frida -U -l _agent.js com.fenfei.syscalldemo

},{"../../frida-syscall-interceptor":4,"./logger":2}],2:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.log = void 0;
function log(message) {
    console.log(message);
}
exports.log = log;

},{}],3:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class SyscallCallback {
    constructor(frida, native) {
        this.frida = frida;
        this.native = native;
    }
}
exports.SyscallCallback = SyscallCallback;

},{}],4:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const callback_1 = require("./callback");
let callbacks = [];
function hookSyscall(syscallAddress, callback) {
    if (Process.arch == 'arm') {
        const address = syscallAddress.sub(8);
        const instructions = address.readByteArray(20);
        if (instructions == null) {
            throw new Error(`Unable to read instructions at address ${address}.`);
        }
        console.log(" ==== old instructions ==== " + address);
        console.log(instructions);
        Memory.patchCode(address, 20, function (code) {
            let writer = null;
            writer = new ArmWriter(code, { pc: address });
            writer.putBranchAddress(createCallback(callback, instructions, address.add(20), syscallAddress));
            writer.flush();
        });
        console.log(" ==== new instructions ==== " + address);
        const instructionsNew = address.readByteArray(20);
        console.log(instructionsNew);
    }
    else {
        const address = syscallAddress.sub(12);
        const instructions = address.readByteArray(12);
        if (instructions == null) {
            throw new Error(`Unable to read instructions at address ${address}.`);
        }
        console.log(" ==== old instructions ==== " + address);
        console.log(instructions);
        Memory.patchCode(address, 16, function (code) {
            let writer = null;
            writer = new Arm64Writer(code, { pc: address });
            writer.putBranchAddress(createCallback(callback, instructions, address.add(16), syscallAddress));
            writer.flush();
        });
        console.log(" ==== new instructions ==== " + address);
        const instructionsNew = address.readByteArray(12);
        console.log(instructionsNew);
    }
}
exports.hookSyscall = hookSyscall;
function createCallback(callback, instructions, retAddress, syscallAddress) {
    // Create custom instructions.
    let frida = Memory.alloc(Process.pageSize);
    Memory.patchCode(frida, Process.pageSize, function (code) {
        let writer = null;
        if (Process.arch == 'arm') {
            writer = new ArmWriter(code, { pc: frida });
            // Restore argument instructions.            
            writer.putBytes(instructions);
            // FE 5F 2D E9 STMFD  SP!, {R1-R12,LR} 寄存器入栈 不存 r0
            // FF 5F 2D E9 STMFD  SP!, {R0-R12,LR} 寄存器入栈  
            writer.putInstruction(0xE92D5FFF);
            // 00 A0 0F E1 MRS R10, CPSR
            // 00 04 2D E9 STMFD SP!, {R10}    // 状态寄存器入栈
            writer.putInstruction(0xE10FA000);
            writer.putInstruction(0xE92D0400);
            // instructions.size = 20  + 5条指令
            writer.putLdrRegAddress("lr", frida.add(20 + 5 * 4));
            writer.putBImm(callback);
            // 00 04 BD E8  LDMFD SP!, {R10}   // 状态寄存器出栈     
            // 0A F0 29 E1  MSR CPSR_cf, R10
            writer.putInstruction(0xE8BD0400);
            writer.putInstruction(0xE129F00A);
            // FE 9F BD E8 LDMFD  SP!, {R1-R12,PC}    寄存器出栈 不存 r0
            // FF 5F BD E8 LDMFD  SP!, {R0-R12,LR}    寄存器出栈 
            writer.putInstruction(0xE8BD5FFF);
        }
        else {
            writer = new Arm64Writer(code, { pc: frida });
            // Restore argument instructions.
            writer.putBytes(instructions);
            // Push all registers except x0.
            writer.putPushRegReg('x15', 'x1');
            writer.putPushRegReg('x2', 'x3');
            writer.putPushRegReg('x4', 'x5');
            writer.putPushRegReg('x6', 'x7');
            writer.putPushRegReg('x8', 'x9');
            writer.putPushRegReg('x10', 'x11');
            writer.putPushRegReg('x12', 'x13');
            writer.putPushRegReg('x14', 'x15');
            writer.putPushRegReg('x16', 'x17');
            writer.putPushRegReg('x18', 'x19');
            writer.putPushRegReg('x20', 'x21');
            writer.putPushRegReg('x22', 'x23');
            writer.putPushRegReg('x24', 'x25');
            writer.putPushRegReg('x26', 'x27');
            writer.putPushRegReg('x28', 'x29');
            writer.putInstruction(0xd53b420f); // 保存状态寄存器
            writer.putPushRegReg('x30', 'x15');
            // Call native.
            writer.putLdrRegAddress('x16', callback);
            writer.putBlrReg('x16');
            // Pop all registers, except x0, so x0 from native call gets used.
            writer.putPopRegReg('x30', 'x15');
            writer.putInstruction(0xd51b420f); // 还原状态寄存器
            writer.putPopRegReg('x28', 'x29');
            writer.putPopRegReg('x26', 'x27');
            writer.putPopRegReg('x24', 'x25');
            writer.putPopRegReg('x22', 'x23');
            writer.putPopRegReg('x20', 'x21');
            writer.putPopRegReg('x18', 'x19');
            writer.putPopRegReg('x16', 'x17');
            writer.putPopRegReg('x14', 'x15');
            writer.putPopRegReg('x12', 'x13');
            writer.putPopRegReg('x10', 'x11');
            writer.putPopRegReg('x8', 'x9');
            writer.putPopRegReg('x6', 'x7');
            writer.putPopRegReg('x4', 'x5');
            writer.putPopRegReg('x2', 'x3');
            writer.putPopRegReg('x15', 'x1');
        }
        // Call syscall.
        // writer.putInstruction(0xd4000001);
        writer.putBranchAddress(retAddress);
        writer.flush();
    });
    console.log("==== frida ====");
    console.log(frida);
    console.log("==== retAddress ====");
    console.log(retAddress);
    // Store callback so it doesn't get garbage collected.
    callbacks.push(new callback_1.SyscallCallback(frida, callback));
    // Return pointer to the instructions.
    return callbacks[callbacks.length - 1].frida;
}

},{"./callback":3}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJhZ2VudC9pbmRleC50cyIsImFnZW50L2xvZ2dlci50cyIsIi4uL2ZyaWRhLXN5c2NhbGwtaW50ZXJjZXB0b3IvZGlzdC9jYWxsYmFjay5qcyIsIi4uL2ZyaWRhLXN5c2NhbGwtaW50ZXJjZXB0b3IvZGlzdC9pbmRleC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7O0FDQUEscUNBQStCO0FBQy9CLCtFQUE4RDtBQUc5RDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0VBcUJFO0FBRUYsMEJBQTBCO0FBRTFCLEdBQUc7QUFDSCxJQUFJLFFBQVEsR0FBRyxNQUFNLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFFLENBQUM7QUFDM0QsSUFBSSxPQUFPLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFHLDhCQUE4QjtBQUV0RSxZQUFHLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO0FBRWxCLHVDQUFXLENBQUMsT0FBTyxFQUFFLElBQUksY0FBYyxDQUFDLFVBQVUsS0FBSyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsS0FBSztJQUMxRSxJQUFJLElBQUksR0FBRyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUM7SUFFbEMsWUFBRyxDQUFDLG9CQUFvQixDQUFDLENBQUM7SUFDMUIsWUFBRyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsQ0FBQztJQUN0QixZQUFHLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxDQUFDO0lBQ3JCLFlBQUcsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLENBQUM7SUFDckIsWUFBRyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsQ0FBQztJQUV0QixPQUFPLENBQUMsQ0FBQztBQUNiLENBQUMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxLQUFLLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDN0MsS0FBSztBQUlMLCtDQUErQzs7Ozs7O0FDbEQvQyxTQUFnQixHQUFHLENBQUMsT0FBZTtJQUMvQixPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0FBQ3pCLENBQUM7QUFGRCxrQkFFQzs7O0FDRkQsWUFBWSxDQUFDO0FBQ2IsTUFBTSxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUUsWUFBWSxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7QUFDOUQsTUFBTSxlQUFlO0lBQ2pCLFlBQVksS0FBSyxFQUFFLE1BQU07UUFDckIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDbkIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7SUFDekIsQ0FBQztDQUNKO0FBQ0QsT0FBTyxDQUFDLGVBQWUsR0FBRyxlQUFlLENBQUM7OztBQ1IxQyxZQUFZLENBQUM7QUFDYixNQUFNLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxZQUFZLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztBQUM5RCxNQUFNLFVBQVUsR0FBRyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7QUFDekMsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO0FBQ25CLFNBQVMsV0FBVyxDQUFDLGNBQWMsRUFBRSxRQUFRO0lBQ3pDLElBQUksT0FBTyxDQUFDLElBQUksSUFBSSxLQUFLLEVBQUU7UUFDdkIsTUFBTSxPQUFPLEdBQUcsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN0QyxNQUFNLFlBQVksR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQy9DLElBQUksWUFBWSxJQUFJLElBQUksRUFBRTtZQUN0QixNQUFNLElBQUksS0FBSyxDQUFDLDBDQUEwQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1NBQ3pFO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsR0FBRyxPQUFPLENBQUMsQ0FBQztRQUN0RCxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzFCLE1BQU0sQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLEVBQUUsRUFBRSxVQUFVLElBQUk7WUFDeEMsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ2xCLE1BQU0sR0FBRyxJQUFJLFNBQVMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxFQUFFLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQztZQUM5QyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLFFBQVEsRUFBRSxZQUFZLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQ2pHLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNuQixDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsOEJBQThCLEdBQUcsT0FBTyxDQUFDLENBQUM7UUFDdEQsTUFBTSxlQUFlLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNsRCxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDO0tBQ2hDO1NBQ0k7UUFDRCxNQUFNLE9BQU8sR0FBRyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3ZDLE1BQU0sWUFBWSxHQUFHLE9BQU8sQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDL0MsSUFBSSxZQUFZLElBQUksSUFBSSxFQUFFO1lBQ3RCLE1BQU0sSUFBSSxLQUFLLENBQUMsMENBQTBDLE9BQU8sR0FBRyxDQUFDLENBQUM7U0FDekU7UUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLDhCQUE4QixHQUFHLE9BQU8sQ0FBQyxDQUFDO1FBQ3RELE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDMUIsTUFBTSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUUsRUFBRSxFQUFFLFVBQVUsSUFBSTtZQUN4QyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDbEIsTUFBTSxHQUFHLElBQUksV0FBVyxDQUFDLElBQUksRUFBRSxFQUFFLEVBQUUsRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDO1lBQ2hELE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLFlBQVksRUFBRSxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLGNBQWMsQ0FBQyxDQUFDLENBQUM7WUFDakcsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ25CLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsR0FBRyxPQUFPLENBQUMsQ0FBQztRQUN0RCxNQUFNLGVBQWUsR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ2xELE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUM7S0FDaEM7QUFDTCxDQUFDO0FBQ0QsT0FBTyxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUM7QUFDbEMsU0FBUyxjQUFjLENBQUMsUUFBUSxFQUFFLFlBQVksRUFBRSxVQUFVLEVBQUUsY0FBYztJQUN0RSw4QkFBOEI7SUFDOUIsSUFBSSxLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDM0MsTUFBTSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLFFBQVEsRUFBRSxVQUFVLElBQUk7UUFDcEQsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLElBQUksT0FBTyxDQUFDLElBQUksSUFBSSxLQUFLLEVBQUU7WUFDdkIsTUFBTSxHQUFHLElBQUksU0FBUyxDQUFDLElBQUksRUFBRSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO1lBQzVDLDZDQUE2QztZQUM3QyxNQUFNLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzlCLGtEQUFrRDtZQUNsRCw4Q0FBOEM7WUFDOUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNsQyw0QkFBNEI7WUFDNUIsNkNBQTZDO1lBQzdDLE1BQU0sQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDbEMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNsQyxpQ0FBaUM7WUFDakMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNyRCxNQUFNLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3pCLGtEQUFrRDtZQUNsRCxnQ0FBZ0M7WUFDaEMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNsQyxNQUFNLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ2xDLHFEQUFxRDtZQUNyRCxnREFBZ0Q7WUFDaEQsTUFBTSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUNyQzthQUNJO1lBQ0QsTUFBTSxHQUFHLElBQUksV0FBVyxDQUFDLElBQUksRUFBRSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO1lBQzlDLGlDQUFpQztZQUNqQyxNQUFNLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzlCLGdDQUFnQztZQUNoQyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNsQyxNQUFNLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNqQyxNQUFNLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNqQyxNQUFNLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNqQyxNQUFNLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNqQyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNuQyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNuQyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNuQyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNuQyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNuQyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNuQyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNuQyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNuQyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNuQyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNuQyxNQUFNLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsVUFBVTtZQUM3QyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNuQyxlQUFlO1lBQ2YsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRSxRQUFRLENBQUMsQ0FBQztZQUN6QyxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3hCLGtFQUFrRTtZQUNsRSxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNsQyxNQUFNLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsVUFBVTtZQUM3QyxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNsQyxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNsQyxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNsQyxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNsQyxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNsQyxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNsQyxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNsQyxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNsQyxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNsQyxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNsQyxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNoQyxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNoQyxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNoQyxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNoQyxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztTQUNwQztRQUNELGdCQUFnQjtRQUNoQixxQ0FBcUM7UUFDckMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3BDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNuQixDQUFDLENBQUMsQ0FBQztJQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsQ0FBQztJQUMvQixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ25CLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUMsQ0FBQztJQUNwQyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3hCLHNEQUFzRDtJQUN0RCxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksVUFBVSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQztJQUNoRSxzQ0FBc0M7SUFDdEMsT0FBTyxTQUFTLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7QUFDakQsQ0FBQyIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIn0=
