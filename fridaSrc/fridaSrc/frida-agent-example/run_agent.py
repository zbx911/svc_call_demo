import frida
import sys
import time
import io
#代码可以直接写入下面，但是可读性不好
jscode = """
Java.perform(function () {

 });
"""
def printMessage(message,data):
    if message['type'] == 'send':
        print('[*] {0}'.format(message['payload']))
        # file_object = open("e:\\log.txt", 'ab+')
        # file_object.write(message['payload'].encode())
        # file_object.write('\n'.encode())
        # file_object.close()
    else:
        print(message)


device = frida.get_usb_device()
front_app = device.get_frontmost_application()
#下面要输入你应用的包名
pid = device.spawn(["com.rom.cpptest"])
device.resume(pid)
#这里必须要有个延迟不然获取不到front_app，时间跟据自己机型调整
time.sleep(18)
front_app = device.get_frontmost_application()


if front_app is  None:
    print("请运行要hook的应用")
    exit(0)


print(front_app)
print(front_app.name)
process = device.attach(front_app.name)

#process = frida.get_usb_device().attach('cpptest')
#直接读入sotest.js，在pycharm中代码有颜色感知，易于编辑
with open('./_agent.js',encoding='utf-8') as f:
    jscode = f.read()
script = process.create_script(jscode)

#script = process.create_script(jscode)
script.on('message',printMessage)
script.load()
sys.stdin.read()