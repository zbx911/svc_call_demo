function hook_check() {
    Java.perform(function () {
        var OkHttpClient = Java.use("okhttp3.OkHttpClient")

        OkHttpClient.newCall.implementation = function (request) {
            var result = this.newCall(request)
            console.log(request.toString())
            return result
        };
        var URL = Java.use('java.net.URL')
        URL.$init.overload('java.lang.String').implementation = function(urlstr){
            console.log('url => ',urlstr)
            var result = this.$init(urlstr)
            return result
        }
        URL.openConnection.overload().implementation = function(){

            var result = this.openConnection()
            console.log('openConnection() returnType =>',result.$className)
            return result
        }
        var HttpURLConnectionImpl = Java.use('com.android.okhttp.internal.huc.HttpURLConnectionImpl')
        HttpURLConnectionImpl.setRequestProperty.implementation = function(key,value){

            var result = this.setRequestProperty(key,value)
            console.log('setRequestProperty => ',key,':',value)
            return result
        }
         //hook so 中的函数 当初始化应用时并没有加载libnative-lib.so，这时会报错，需要点击一下“root检测按”钮才可以
        //findExportByName 的两个参数 libnative-lib.so 和 Java_com_rom_cpptest_MainActivity_detectRoot都要改一下名

        // Interceptor.attach(Module.findExportByName("libnative-lib.so", "Java_com_rom_cpptest_MainActivity_detectRoot_1svc"),
        //     {
        //     onEnter: function (args) {
        //         send("Hook detectRoot start");
        //         send("args[0]=" + args[0]);
        //         send("args[1]=" + args[1]);
        //     },
        //     onLeave: function (retval) {
        //         send("ori detectRoot return:" + retval);
        //          retval.replace(0);
        //          send("changed add return:" + retval);
        //     }
        // });

        //hook so 中的函数
        // Interceptor.attach(Module.findExportByName("libnative-lib.so", "Java_com_rom_cpptest_MainActivity_add"), {
        //     onEnter: function (args) {
        //         send("Hook add start");
        //         send("args[0]=" + args[0]);
        //         send("args[1]=" + args[1]);
        //         send("args[2]=" + args[2]);
        //         send("args[3]=" + args[3]);
        //     },
        //     onLeave: function (retval) {
        //         send("ori add return:" + retval);
        //         retval.replace(100);
        //         send("changed add return:" + retval);
        //     }
        // });

     // Java.perform(function(){
     //        var TestSig = Java.use("com.rom.cpptest.MainActivity"); // 类的加载路径
     //
     //      TestSig.onCreate.overload('android.os.Bundle').implementation = function(str){
     //        send("success1");
     //        this.onCreate(str);
     //        send("success2");
     //    };
     //    });

        //com.rom.cpptest.MainActivity$b 这个$b需要用反编译软件jade进行逆向才可以确定
        //调试没加混淆时这个类

   //      var button=  Java.use('com.rom.cpptest.MainActivity$b');
   //      button.onClick.implementation =   function(v) {
   //          send("button.onClick.implementation input v ="+v);
   //          //注释下面代码后将会不执行任何操作
   //          this.onClick(v);
   //          console.log('onClick twice')
   //          //this.onClick(v);
   //          //打印的堆栈与上面相吻合
   //          //console.log('current stack \n '+printstack());
   //      };
   // //显示类的所有函数
   //      var clazz = Java.use("com.rom.cpptest.MainActivity$b");
   //      var methods = clazz.class.getDeclaredMethods();
   //      if(methods.length > 0){
   //          console.log("getDeclaredMethods of class '" + methods.length + "':");
   //          methods.forEach(function(method){
   //              console.log(method);
   //          });
   //          }
   //
   //      //hook 按钮函数
   //       Java.choose('com.rom.cpptest.MainActivity$b', {
   //          onMatch: function (instance) {
   //              console.log('button found', instance)
   //              send("v ="+instance  + "\nthis="+this);
   //
   //              //var view= Java.use("android.view.View");
   //              //instance.onClick(view);
   //          },
   //          onComplete: function () {
   //              console.log('search Complete')
   //          }
   //      })
        //获取加载的所有模块
        // Process.enumerateModules({
        //     onMatch: function (exp) {
        //         if (exp.name == 'libnative-lib.so') {
        //             //send(exp.name + "|" + exp.base + "|" + exp.size + "|" + exp.path);
        //             console.log(exp.name + " : " +  exp.base + "\t" + exp.size + "\t" + exp.path);
        //             //send(exp);
        //             //return 'stop';
        //         }
        //     },
        //     onComplete: function () {
        //         send('stop');
        //     }
        // });

        // var resultPtr = null;
        // Interceptor.attach(Module.findExportByName("libnative-lib.so", "Java_com_rom_cpptest_MainActivity_stringFromJNI"), {
        //     onEnter: function (args) {
        //         send("Hook getString start");
        //         send("args[0]=" + args[0]);
        //         send("args[1]=" + args[1]);
        //         resultPtr = args[1];
        //         //printstack()
        //     },
        //     onLeave: function (retval) {
        //         var string_to_send = retval.toString();
        //         send("retval value :" + retval.toString());
        //         send("getString return:" + string_to_send);
        //         //上面已经写了怎么Hook修改native层函数返回值为int类型的情况，使用replace()函数直接修改即可，
        //         // 但是返回情况为字符串则不一样，在c语言中，返回值为字符串其实是返回了一个char *（字符串指针），
        //         // 所以简单的替换是无法取效果的，具体怎么修改返回值，接着看下面
        //         var env = Java.vm.getEnv(); //获取env对象，即第一个参数
        //         var jstrings = env.newStringUtf("少年强则中国强"); //返回的是字符串指针，构造一个newStringUtf对象用来代替这个指针
        //         send("函数返回new值：" + jstring2Str(retval));
        //         console.log("函数返回new值 =>",Java.vm.getEnv().getStringUtfChars(retval, null).readCString())
        //         send("改变jstrings：" + jstring2Str(jstrings));
        //         //var my_string = Java.use("java.lang.String").overload('java.lang.String').$new(jstrings);
        //         //send("my_string my_string:" + my_string );
        //
        //         retval.replace(jstrings); //替换返回值
        //
        //         var buffer = Memory.readByteArray(resultPtr, 64);
        //         console.log(hexdump(buffer, {
        //             offset: 0,
        //             length: 64,
        //             header: true,
        //             ansi: false
        //         }));
        //     }
        // });


        // Java.choose('com.rom.cpptest.MainActivity$2', {
        //     onMatch: function (instance) {
        //         console.log('button found', instance)
        //     },
        //     onComplete: function () {
        //         console.log('search Complete')
        //     }
        // })


        // var MainAcitivity = Java.use('com.rom.cpptest.MainActivity')
        // console.log("Java.Use.Successfully!") //定位类成功！
        // // 静态函数主动调用
        // MainAcitivity.add_log("helloworld");
        //
        // Java.choose('com.rom.cpptest.MainActivity', {
        //     onMatch: function (instance) {
        //         console.log('MainActivity instance found', instance)
        //         console.log("static helloworld: " + instance.add_log("helloworld"));
        //         console.log("static helloworld: " + instance.add_non_static_log("add_non_static_log"));
        //         //instance.secret()
        //         //instance.staticSecret();
        //     },
        //     onComplete: function () {
        //         console.log('search Complete')
        //     }
        // })

        //因为生成类的实例是在按钮之后，所以一启动应用并没有生成实例，会报错
        // Java.choose('com.rom.cpptest.MainActivity$cinc', {
        //     onMatch: function (instance) {
        //         console.log('cinc instance found', instance)
        //         console.log("Result of cinc add func: " + instance.add(7, 3));
        //         //instance.secret()
        //         //instance.staticSecret();
        //     },
        //     onComplete: function () {
        //         console.log('search Complete')
        //     }
        // })


        // Interceptor.attach(Module.findExportByName("libc.so", "open"), {
        //     onEnter: function (args) {
        //
        //         send("open called! args[0]:", Memory.readByteArray(args[0], 256));
        //     },
        //     onLeave: function (retval) {
        //
        //     }
        // });


    });

//把javascripts中的string 转成可以输出的string用于观察变量
    function jstring2Str(jstring) { //从frida_common_funs.js中copy出来
        var ret;
        Java.perform(function () {
            var String = Java.use("java.lang.String");
            ret = Java.cast(jstring, String);//jstring->String
        });
        return ret;
    }

    function printstack() {
        //方式1 ，直接抛
        //send(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
        //方式2 ，逐行打印
        var stack = Java.use("java.lang.Thread").$new().currentThread().getStackTrace();
        for (var i = 2; i < stack.length; i++) {
            send("getStackTrace[" + (i - 2) + "] : " + stack[i].toString());
        }
    }
}

function main() {
    hook_check();
}

setImmediate(main);